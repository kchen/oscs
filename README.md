# OSCs

# Data and code for 'Reorganization energies of flexible organic molecules as a challenging target for machine learning enhanced virtual screening' 

dataframe/Introduction_to_load_data.ipynb - Dataframe tutorial

code/ML/ML_Kmax.ipynb - Testing Kmax for the Autobag representation

code/ML/SOAP_parameter_optimization.ipynb - Testing the SOAP hyperparameters

code/ML/ML_str.ipynb - Build a structure-based GPR model to predict reorganization energies

code/ML/ML_str_delta.ipynb - Build a structure-based GPR model with the aid of delta-learning

code/ML/ML_pro_delta.ipynb - Build a electronic property-based GPR model with the aid of delta-learning

code/ML/ML_sp_delta.ipynb - Build a structure and property-based GPR model with the aid of delta-learning

code/substructure/Sub_generation.ipynb - Getting high enrichment substructures

code/substructure/complete_fragment.ipynb - Finding promising fragment based on substructure search
