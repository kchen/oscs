from __future__ import print_function
import numpy as np
from sklearn.cluster import KMeans, MiniBatchKMeans
from dscribe.descriptors import SOAP
from sklearn.base import BaseEstimator

#more detail about this method, please refer to the paper 'Machine-learning  enhanced  global  optimization  by  clustering  local  envi-ronments  to  enable  bundled  atomic  energie'
class BaseLern(BaseEstimator):
    def __init__(self,
                estimator:BaseEstimator,
                **fit_kwargs
                ) -> None:
        self.estimator = estimator
        
    def fit(self,x,**fit_kwargs):
        flatvs,molids = self.flatvec(x)
        self.estimator.fit(flatvs,**fit_kwargs)
        return self
    
    def predict(self,x,**predict_kwargs):
        flatvs,molids = self.flatvec(x)
        labels =  self.estimator.predict(flatvs,**predict_kwargs)
        vecs = self.vecs_train(x,molids,labels)
        return vecs            
                 
class autobag(BaseLern):
    def __init__(self,
                 estimator:BaseEstimator,
                 kmax=500,
                 rcut=3.5,
                 atomsigma=0.35,
                 **fit_kwargs) -> None:
        super(autobag,self).__init__(estimator,**fit_kwargs)
        self.kmax = kmax
        self.rcut = rcut
        self.atomsigma = atomsigma

    def soap_setup(self,nmax=6,lmax=8,species=['C','H']):
        species = species
        rcut = self.rcut
        nmax = nmax
        lmax = lmax

    # Setting up the SOAP descriptor
        soap = SOAP(
            species=species,
            periodic=False,
            rcut=self.rcut,
            nmax=nmax,
            lmax=lmax,
            sigma=self.atomsigma,
            average='off'
        )
        return soap  
    def generate_soapvec(self,mol,soap):
        soapvec = soap.create(mol)
        return soapvec
    def flatvec(self,mols):
        soap = self.soap_setup()
        flatvs = []
        molids = []
        for i,mol in enumerate(mols):
            soapvec = self.generate_soapvec(mol,soap)
            flatvs.extend(soapvec)
            molids.extend([i]*len(soapvec))
        return flatvs, molids        
    def vecs_train(self,mols,molids,labels,normalize=True,bitvec=False):
        mol_fvecs = np.zeros((len(mols),self.kmax))
        for i,molid in enumerate(molids):   
            k = labels[i]
            if bitvec:
                mol_fvecs[molid][k] = 1
            else:
                mol_fvecs[molid][k] += 1
        if normalize:
            norms = np.linalg.norm(mol_fvecs,axis=1)
            return np.array([mol_fvecs[i]/norms[i] for i in range(len(norms))])
        return mol_fvecs 
    