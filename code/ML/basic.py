import numpy as np
import pandas as pd
import ase
from ase import atoms
from ase.io import *

def df_dft_lambdas(df):
    lambdas = []
    for index, row in df.iterrows():
        elamb = row.lambdas_dft_meV
        lambdas.append(elamb)
    return lambdas

def df_gfn_lambdas(df):
    lambdas = []
    for index, row in df.iterrows():
        elamb = row.lambdas_gfn_meV
        lambdas.append(elamb)
    return lambdas

def df_mol_n(df):
    mols = []
    for index, row in df.iterrows():
        string = str(row.gfn_origin_xyz)
        with open('large_data.txt','w') as f:
            f.write(string)
        mol = read('large_data.txt',format='xyz')
        mols.append(mol)
    return mols

def calc_RMSE(predict,ref):
    err = (predict-ref)
    RMSE = np.sqrt(np.mean(err**2))
    return RMSE

def calc_MAE(predict,ref):
    err = (predict-ref)
    MAE = np.mean(np.abs(err))
    return MAE

def df_G_gfn(df):
    properties = []
    for index, row in df.iterrows():
        g_n1 = row.gfn_n_HOMO_eV
        g_n2 = row.gfn_nc_HOMO_eV
        g_n3 = row.gfn_c_HOMO_eV
        g_n4 = row.gfn_cn_HOMO_eV
        g_n5 = row.gfn_n_LUMO_eV
        g_n6 = row.gfn_nc_LUMO_eV
        g_n7 = row.gfn_c_LUMO_eV
        g_n8 = row.gfn_cn_LUMO_eV  
        g_n9 = row.gfn_n_GAP_eV
        g_n10 = row.gfn_nc_GAP_eV
        g_n11 = row.gfn_c_GAP_eV
        g_n12 = row.gfn_cn_GAP_eV
        g_n13 = row.gfn_n_Energy_Eh
        g_n14 = row.gfn_nc_Energy_Eh
        g_n15 = row.gfn_c_Energy_Eh
        g_n16 = row.gfn_cn_Energy_Eh
        g_n17 = row.gfn_n_Fermi_eV
        g_n18 = row.gfn_nc_Fermi_eV
        g_n19 = row.gfn_c_Fermi_eV
        g_n20 = row.gfn_cn_Fermi_eV        
        g_n21 = g_n14 - g_n13
        g_n22 = g_n16 - g_n15
        pro = [g_n1,g_n2,g_n3,g_n4,g_n5,g_n6,g_n7,g_n8,g_n9,g_n10,g_n11,g_n12,g_n13,g_n14,g_n15,g_n16,g_n17,g_n18,g_n19,g_n20,g_n21,g_n22]
        properties.append(pro)
    return np.vstack(properties)

