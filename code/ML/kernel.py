import numpy as np
import pandas as pd
import ase
from ase import atoms
from ase.io import *
from matplotlib import pyplot as plt
from sklearn.gaussian_process.kernels import RBF
from numpy.linalg import inv
from scipy.linalg import cholesky, cho_solve, solve_triangular
from scipy.optimize import minimize

def K1(x1,x2,l1,l2,s1,s2):
    kernel1 = l1**2 * RBF(length_scale=s1)
    kernel2 = l2**2 * RBF(length_scale=s2)
    k1 = kernel1(x1,x1)
    k2 = kernel2(x2,x2)
    k = k1 + k2
    return k

def log_likelihood(y, x1,x2,s1,s2,l1,l2, n):
    K = (K1(x1,x2,l1,l2,s1,s2)) + n * np.eye(len(x1))
    L = cholesky(K, lower=True)
    y_train = y[:, np.newaxis]
    alpha = cho_solve((L, True), y_train)
    log_likelihood_dims = -0.5 * np.einsum("ik,ik->k", y_train, alpha)
    log_likelihood_dims -= np.log(np.diag(L)).sum()
    log_likelihood_dims -= K.shape[0] / 2 * np.log(2 * np.pi)
    log_likelihood = log_likelihood_dims.sum(-1)
    return -log_likelihood

def optimal_hyperparameters22(y, x1,x2,s1,s2,l1,l2,n):
    result = minimize(lambda hyper: log_likelihood(y, x1, x2, hyper[0], hyper[1], hyper[2], hyper[3], hyper[4]), [s1,s2,l1,l2,n], bounds=((1e-3,5e3),(1e-3, 5e3),(1e-3,5e3),(1e-3, 5e3),(1e-10, 1e4)), method='L-BFGS-B')
    return result.x[0],result.x[1],result.x[2],result.x[3],result.x[4]



def predict(x_train1, x_train2,y_train,x_pred1,x_pred2,s1,s2,l1,l2,n):
    def k_pred(x1,x2,x3=None,x4=None):
        kernel1 = l1**2 * RBF(length_scale=s1)
        kernel2 = l2**2 * RBF(length_scale=s2)
        if x3 is None:
            k1 = kernel1(x1,x1)
            k2 = kernel2(x2,x2)
            k = k1 + k2        
        else:
            k1 = kernel1(x1,x3)
            k2 = kernel2(x2,x4)
            k = k1 + k2
        return k
    K_tt = k_pred(x_train1,x_train2) + n* np.eye(len(x_train1))
    K_tT = k_pred(x_pred1,x_pred2, x_train1,x_train2)
    K_TT = k_pred(x_pred1,x_pred2) + n* np.eye(len(x_pred1))
    L = cholesky(K_tt, lower=True)
    alpha = cho_solve((L, True), y_train)
    y_mean = K_tT.dot(alpha)
    Lk = np.linalg.solve(L, K_tT.T)
    s2 = np.diag(K_TT) - np.sum(Lk**2, axis=0)
    stdv = np.sqrt(s2)
    return y_mean, stdv
