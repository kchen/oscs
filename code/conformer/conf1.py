import os
import ase
from ase import atoms
from ase.io import *
from rdkit import Chem
from rdkit.Chem import AllChem

#read the SMILE, the SMILES can be replaced as you want
s = 'c1ccc2ccccc2c1'
#add hydrogen
mol = Chem.MolFromSmiles(s)
mol = Chem.AddHs(mol)
#transfer to SDF
writer = Chem.SDWriter('conversion.sdf')
writer.write(mol)
writer.close()
#use xtb/gfn2 to generate 3D initial geometry from 2D
os.system('xtb conversion.sdf --gfn 2 --sp')
#read the 3D geometry
ase_mol = read('gfnff_convert.sdf',format='sdf')
#transfer SDF to xyz (this is not mandatory, xtb can also reconigize SDF file)
ase_mol.write('scartch.xyz')
#optimize the structure by gfn2 before doing the conformer search, the optmized strcture is 'xtbopt.xyz', which will be used for the next step
os.system('xtb scartch.xyz --opt --gfn 2')
#do the conformer search with CREST
os.system('crest xtbopt.xyz -gfn 1 -T 8')
#the CREST default best conformer can be obtained, which is named 'crest_best.xyz'. 

