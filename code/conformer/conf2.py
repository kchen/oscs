import os
import ase
from ase import atoms
from ase.io import *
from rdkit import Chem
from rdkit.Chem import AllChem

#read the SMILE, the SMILES can be replaced as you want
s = 'c1ccc2ccccc2c1'
#add hydrogen
mol = Chem.MolFromSmiles(s)
mol = Chem.AddHs(mol)
#transfer to SDF
writer = Chem.SDWriter('conversion.sdf')
writer.write(mol)
writer.close()
#use xtb/gfn2 to generate 3D initial geometry from 2D
os.system('xtb conversion.sdf --gfn 2 --sp')
#read the 3D geometry
ase_mol = read('gfnff_convert.sdf',format='sdf')
#transfer SDF to xyz (this is not mandatory, xtb can also reconigize SDF file)
ase_mol.write('scartch.xyz')
#optimize the structure by gfn2 before doing the conformer search, the optmized strcture is 'xtbopt.xyz', which will be used for the next step
os.system('xtb scartch.xyz --opt --gfn 2')
#do the conformer search with CREST
os.system('crest xtbopt.xyz -gfnff -T 8 -quick')
#the CREST default best conformer can be obtained, which is named 'crest_best.xyz'. But we reoptimize the best conformer with gfn1 to get the finial gfnff-crest best conformer. 
os.system('xtb crest_best.xyz --opt --gfn 1')
#get the best conformer
last_mol = read('xtbopt.xyz')
last_mol.write('last_conformer.xyz')
#here the reorganization energy can also be calculated for example:
#from xtb_lambda import lam_cal
#E_lambda = lam_cal(last_mol, idx=0)