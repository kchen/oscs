import os
import ase
from ase import atoms
from ase.io import *
from rdkit import Chem
from rdkit.Chem import AllChem

#read the SMILE, the SMILES can be replaced as you want
s = 'c1ccc2ccccc2c1'
#add hydrogen
mol = Chem.MolFromSmiles(s)
mol = Chem.AddHs(mol)
#transfer to SDF
writer = Chem.SDWriter('conversion.sdf')
writer.write(mol)
writer.close()
#use xtb/gfn2 to generate 3D initial geometry from 2D
os.system('xtb conversion.sdf --gfn 2 --sp')
#read the 3D geometry
ase_mol = read('gfnff_convert.sdf',format='sdf')
#transfer SDF to xyz (this is not mandatory, xtb can also reconigize SDF file)
ase_mol.write('scartch.xyz')
#optimize the structure by gfn2 before doing the conformer search, the optmized strcture is 'xtbopt.xyz', which will be used for the next step
os.system('xtb scartch.xyz --opt --gfn 2')
#do the conformer search with CREST
os.system('crest xtbopt.xyz -gfnff -T 8 -quick')
#read all generated conformers
con = read('crest_conformers.xyz', index=':', format='xyz')
#optmize each conformer in gfn1 level 
energy = []
ase_str = []
for index,mol in enumerate(con):
    os.mkdir('run_{}'.format(index))
    os.chdir('run_{}'.format(index))
    mol.write('scratch.xyz')
    os.system('xtb scratch.xyz --opt --gfn 1 > E0G0.log')
    #extract energy
    os.system('cat E0G0.log | grep TOTAL >> energies.txt')
    with open('energies.txt') as out:    
        for line in out.readlines():
            E0G0=line.split('TOTAL ENERGY')[1].split('Eh')[0]  
    energy.append(float(E0G0))
    ase_atoms=read('xtbopt.xyz')
    ase_str.append(ase_atoms)
    os.chdir('..')
#sort based on energy
seq_index = np.argmin(energy)
#get the best conformer based on gfn1 energy re-ranking
last_mol = ase_str[index]
#save the last conformers
last_mol.write('last_conformer.xyz')
