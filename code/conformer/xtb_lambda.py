import os,shutil, sys
from copy import deepcopy
from ase.io import *
import numpy as np

def lam_cal(ase_atoms, method='GFN1', idx=0, optlevel='vtight'):
    #cwd=os.getcwd()
    #cmd = 'xtb scratch.xyz --gfn {} '.format(method.split('GFN')[1])
    os.mkdir('run_{}'.format(idx))
    os.chdir('run_{}'.format(idx))
    ase_atoms.write('scratch.xyz')
    os.system('xtb scratch.xyz --gfn {} --opt {} --chrg 0 > E0G0.log'.format(method.split('GFN')[1], optlevel))
    os.system('cat E0G0.log | grep TOTAL >> energies.txt')
    os.system('xtb xtbopt.xyz --gfn {} --chrg 1 --uhf 1 > E0Gplus.log'.format(method.split('GFN')[1]))
    os.system('cat E0Gplus.log | grep TOTAL >> energies.txt')
    os.system('xtb scratch.xyz --gfn {} --opt {} --chrg 1 --uhf 1 > EplusGplus.log'.format(method.split('GFN')[1], optlevel))
    os.system('cat EplusGplus.log | grep TOTAL >> energies.txt')
    os.system('xtb xtbopt.xyz --gfn {} --chrg 0 > EplusG0.log'.format(method.split('GFN')[1]))
    os.system('cat EplusG0.log | grep TOTAL >> energies.txt')
    energy = []
    with open('energies.txt') as out:    
        for line in out.readlines():
            E0G0=line.split('TOTAL ENERGY')[1].split('Eh')[0]
            energy.append(float(E0G0))
    energy = np.hstack(energy)
    lamda = (energy[3:4] - energy[:1] + energy[1:2] - energy[2:3]) * 27.2113845 * 1000
    #os.system('echo "{} {}" >> ../lam.txt'.format(float(lamda), idx))
    os.chdir('..')
    os.system('rm -r run_{}'.format(idx))
    return float(lamda)

