from __future__ import print_function
import sys
import numpy as np
import pandas as pd
import ase
from ase import atoms
from ase.io import *
from rdkit.Chem.AllChem import GetMorganFingerprintAsBitVect, GetMorganFingerprint
from rdkit import Chem
from rdkit.Chem import Draw

def SubMatch(mol,child):
    Smi = []
    Ind = []
    for j in child:
        index = []
        smile = []
        for i,s in enumerate(mol):
            parent = Chem.MolFromSmiles(s)
            if parent.HasSubstructMatch(j):
                smile.append(s)
                index.append(i)
        Smi.append(smile)
        Ind.append(index)    
    return Smi, Ind

def complete_ratio(small_str, all_str, child):
    smi_small, Ind_small = SubMatch(small_str, child)
    smi_all, Ind_all = SubMatch(all_str, child)
    ratio = [(len(Ind_small[i])/len(small_str))/(len(Ind_all[i])/len(all_str)) for i in range(len(child))]
    return ratio


