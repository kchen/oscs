from __future__ import print_function
from collections import defaultdict, Counter
from openbabel import openbabel
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import rdDepictor
from rdkit.Chem.Draw import rdMolDraw2D
from rdkit.Chem import Draw
from rdkit.Chem.AllChem import GetMorganFingerprintAsBitVect, GetMorganFingerprint
from IPython.display import SVG
import numpy as np
import pandas as pd
import ase
from ase import atoms
from ase.io import *

def Smile_generation(xyz):
    from openbabel import openbabel
    smile_list = []
    for i,value in enumerate(xyz):
        value.write('initial.xyz', format='xyz')
        obConversion = openbabel.OBConversion()
        obConversion.SetInAndOutFormats("xyz","smi")
        mol = openbabel.OBMol()
        obConversion.ReadFile(mol,'initial.xyz')
        Smile = obConversion.WriteString(mol)
        smile_list.append(Smile)  
    return smile_list

def small_str(df, a=100):
    mols = []
    lambdas = []
    bool_series = pd.notnull(df["lambdas_dft_meV"])
    df_new1 = df[bool_series]
    for index, row in df_new1.iterrows():
        if row.lambdas_dft_meV is not None:
            elamb = row.lambdas_dft_meV
            lambdas.append(elamb)
            string = str(row.gfn_origin_xyz)
            with open('large_data.txt','w') as f:
                f.write(string)
            mol = read('large_data.txt',format='xyz')
            mols.append(mol)
    ind = np.argsort(lambdas)
    lambdas_last = np.array(lambdas)[ind][:100] 
    mols_last = np.array(mols)[ind][:100]
    return mols_last, lambdas_last

def all_str(df):
    mols = []
    for index, row in df.iterrows():
        string = str(row.gfn_origin_xyz)
        with open('large_data.txt','w') as f:
            f.write(string)
        mol = read('large_data.txt',format='xyz')
        mols.append(mol)
    mols_s = Smile_generation(mols)
    return mols_s

def substructure(smiles,radii={3,4,5}):
    counts = Counter()
    radii = radii
    S = {}
    mount = {}
    for i,mol in enumerate(smiles):
        M = Chem.MolFromSmiles(mol)    
        info={}
        fp = GetMorganFingerprint(M, 5, bitInfo=info,invariants=AllChem.GetConnectivityInvariants(M,includeRingMembership=False))
        #only choose the radius in {3,4,5} for example
        sub = dict((k,v) for k,v in info.items() if v[0][1] in radii)
        #count the time for IDs
        for key in sub.keys():
            counts[key] += 1    
        #substrucutre store
        amap = {}
        Smiles = {k:Chem.MolToSmiles(Chem.PathToSubmol(M,Chem.FindAtomEnvironmentOfRadiusN(M,v[0][1],v[0][0]),atomMap=amap)) for k,v in sub.items()}
        S.update(Smiles) 
    return counts,S,len(smiles)

def Best_sub_in_total(xyz_pred,xyz_ref,return_best=True):
    counts_ref, smile_ref, L_ref = substructure(xyz_ref)
    counts_pred, smile_pred, L_pred = substructure(xyz_pred)
    counts_r = {k:(v/L_ref) for k,v in counts_ref.items() if v>7}
    smile_r = {k:smile_ref[k] for k in counts_r.keys()}
    counts_total = {k:counts_pred[k] for k in counts_r.keys()}
    counts_t = {k:(v/L_pred) for k,v in counts_total.items()} 
    f_ref = counts_r.values()
    f_all = counts_t.values()
    p = [a / b for a, b in zip(f_ref, f_all)]
    percentage = dict(zip(counts_r.keys(), p))
    Smiles = []
    if return_best:
        keymax = max(percentage,key=percentage.get)
        best_percentage = max(percentage.values())
        Smiles = (smile_r[keymax])
        
    #get the best 50
    else:
        keymax = sorted(percentage, key=percentage.get,reverse=True)[:50]
        best_percentage = sorted(percentage.values(),reverse=True)[:50]
        for s in keymax:
            Smiles.append(smile_r[s])
            
    return best_percentage, keymax, Smiles,L_ref,L_pred

def Best_str_to_draw(smile,key,molSize=(450,200)):
    radii={3,4,5}
    for i,mol in enumerate(smile):
        M = Chem.MolFromSmiles(mol)    
        info={}
        fp = GetMorganFingerprint(M, 5, bitInfo=info,invariants=AllChem.GetConnectivityInvariants(M,includeRingMembership=False))
        #only choose the radius in {1,2,3} for example
        sub = dict((k,v) for k,v in info.items() if v[0][1] in radii)
        if key in sub.keys():
            env = Chem.FindAtomEnvironmentOfRadiusN(M,sub[key][0][1],sub[key][0][0])
            atomsToUse=[]
            for b in env:
                atomsToUse.append(M.GetBondWithIdx(b).GetBeginAtomIdx())
                atomsToUse.append(M.GetBondWithIdx(b).GetEndAtomIdx())
                atomsToUse = list(set(atomsToUse)) 
            
            drawer = rdMolDraw2D.MolDraw2DSVG(molSize[0],molSize[1])
            drawer.DrawMolecule(M,highlightAtoms=atomsToUse,highlightAtomColors={sub[key][0][0]:(0.3,0.3,1)})
            drawer.FinishDrawing()
            svg = drawer.GetDrawingText()
            break
    #return SVG(svg)
    return SVG(svg.replace('svg:',''))

def Best_str_lambda_return(smile,key,lambdas):
    radii={3,4,5}
    lamb = []
    Smi = []
    for i,mol in enumerate(smile):
        M = Chem.MolFromSmiles(mol)    
        info={}
        fp = GetMorganFingerprint(M, 5, bitInfo=info,invariants=AllChem.GetConnectivityInvariants(M,includeRingMembership=False))
        #only choose the radius in {1,2,3} for example
        sub = dict((k,v) for k,v in info.items() if v[0][1] in radii)
        if key in sub.keys():
            lamb.append(lambdas[i])
            Smi.append(smile[i])
    return Smi, lamb

